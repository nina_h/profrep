<tool id="dante" name="Domain based ANnotation of Transposable Elements - DANTE" version="1.0.0">
  <requirements>
    <requirement type="package">last</requirement>
    <requirement type="package">numpy</requirement>
    <requirement type="package" version="1.0.0">dante</requirement>
  </requirements>
  <stdio>
    <regex match="Traceback" source="stderr" level="fail" description="Unknown error" />
    <regex match="error" source="stderr" level="fail" description="Unknown error" />
  </stdio>
<description> Tool for annotation of transposable elements based on the similarity to conserved protein domains database. </description>
<command>
python3 ${__tool_directory__}/dante.py --query ${input} --domain_gff ${DomGff}
	--protein_database ${__tool_data_path__ }/protein_domains/${db_type}_pdb
	--classification ${__tool_data_path__ }/protein_domains/${db_type}_class
</command>
<inputs>
 <param format="fasta" type="data" name="input" label="Choose your input sequence" help="Input DNA must be in proper fasta format, multi-fasta containing more sequences is allowed" />

 <param name="db_type" type="select" label="Select taxon and protein domain database version (REXdb)" help="">
   <options from_file="rexdb_versions.txt">
     <column name="name" index="0"/>
     <column name="value" index="1"/>
   </options>
 </param>

</inputs>

<outputs>
 <data format="gff3" name="DomGff" label="Unfiltered GFF3 file of ALL protein domains from dataset ${input.hid}" />
</outputs>

 <help>

THIS IS A PRIMARY OUTPUT THAT SHOULD UNDERGO FURTHER QUALITY FILTERING TO GET RID OFF POTENTIAL FALSE POSITIVE DOMAINS

**WHAT IT DOES**

This tool uses external aligning programme `LAST`_ and RepeatExplorer database of TE protein domains(REXdb) (Viridiplantae and Metazoa)

.. _LAST: http://last.cbrc.jp/  

*Lastal* runs similarity search to find hits between query DNA sequence and our database of protein domains from all Viridiplantae repetitive elements. Hits with overlapping positions in the sequence (even through other hits) forms a cluster which represents one potential protein domain. Strand orientation is taken into consideration when forming the clusters which means each cluster is built from forward or reverse stranded hits exclusively. The clusters are subsequently processed separately; within one cluster positions are scanned base-by-base and classification strings are assigned for each of them based on the database sequences which were mapped on that place. These asigned classification strings consist of a domain type as well as class and lineage of the repetitive element where the database protein comes from. Different classification levels are separated by "|" character. Every hit is scored according to the scoring matrix used for DNA-protein alignment (BLOSUM80). For single position only the hits reaching certain percentage (80% by default) of the overall best score within the whole cluster are reported. One cluster of overlapping hits represents one domain region and is recorded as one line in the resulting GFF3 file. Regarding the classition strings assigned to one region (cluster) there are three situations that can occur:

	1. There is a single classification string assigned to each position as well as classifications along all the positions in the region are mutually uniform, in this case domain's final classification is equivalent to this unique classification.
	2. There are multiple classification strings assigned to one cluster, i.e. one domain, which leads to classification to the common (less specific) level of all the strings
	3. There is a conflict at the domain type level, domains are reported with slash (e.g. RT/INT) and the classification is in this case ambiguous
	
**There are 2 outputs produced by this tool:**
	
1. GFF3 file of all proteins domains built from all hits found by LAST. Domains are reported per line as regions (start - end) on the original DNA sequence including the seq ID, alignment score and strand orientation. The last "Attributes" column contains several semicolon-separated information related to annotation, repetitive classification, alignment and its quality. This file can undergo further filtering using *Protein Domain Filter* tool

- Attributes reported always:

	Name
		type of domain; if ambiguous reported with slash 
		
	Final_classification 
		definite classification based on all partial classifications of Region_hits_classifications attribute or 
		"Ambiguous_domain" when there is an ambiguous domain type 
		
	Region_Hits_Classifications
		all hits classifications (comma separated) from a certain domain region that reach the set score threshold; in case of multiple annotations the square brackets indicate the number of bases having this particular classification		
			
- Attributes only reported in case of unambiguous domain type (all the attributes including quality information are related to the Best_Hit of the region):
	
	Best_hit  
		classification and position of the best alignment with the highest score within the cluster; in the square brackets is the percentage of the whole cluster range that this best hit covers
		
	Best_Hit_DB_Pos
		showing which part of the original datatabase domain corresponding to the Best Hit was aligned on query DNA (e.g. **Best_Hit_DB_Pos=17:75of79** means the Best Hit reported in GFF represents region from 17th to 75th of total 79 aminoacids in the original domain from the database)
		
	DB_Seq 
		database protein sequence of the best hit mapped to the query DNA
		
	Query_Seq 
		alignment sequence of the query DNA for the best hit	
		
	Identity
		ratio of identical amino acids in alignment sequence to the length of alignment
		
	Similarity
		ratio of alignment positions with positive score (according to the scoring matrix) to the length of alignment
		
	Relat_Length
		ratio of gapless length of the aligned protein sequence to the whole length of the database protein 
		
	Relat_Interruptions
		number of the interruptions (frameshifts + stop codons) in aligned translated query sequence per each starting 100 AA
		
	Hit_to_DB_Length
		proportion of alignment length to the original length of the protein domain from database
	
	

!NOTE: Tool can in average process 0.5 Gbps of the DNA sequence per day. This is only a rough estimate and it is highly dependent on input data (repetive elements occurence) as well as computing resources. Maximum running time of the tool is 7 days.

 </help>
</tool>

